import Vue from 'vue'
import VueRouter from 'vue-router'
import FrontLayout from '@/layout/FrontLayout'
import Home from '@/views/Home'
import Livros from '@/views/Livros'
import Capitulo from '@/views/Capitulo'
import Erro404 from '@/views/Erro404'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: FrontLayout,
    redirect: 'inicio',
    children: [
      {
        path: '',
        name: 'inicio',
        component: Home
      },
      {
        path: 'livros',
        name: 'livros',
        component: Livros
      },
      {
        path: 'add-livros',
        name: 'add-livros',
        component: () => import(/* webpackChunkName: "demo" */ '@/views/AddLivro.vue')
      },
      {
        path: 'edit-livros/:id',
        name: 'edit-livros',
        component: () => import(/* webpackChunkName: "demo" */ '@/views/EditarLivro.vue')
      },
      {
        path: 'livro/:id/capitulo',
        name: 'capitulo',
        component: Capitulo
      },
      {
        path: 'livro/:id/add-capitulo',
        name: 'add-capitulo',
        component: () => import(/* webpackChunkName: "demo" */ '@/views/AddCapitulo.vue')
      },
      {
        path: 'livro/:id/edit-capitulo/:cap',
        name: 'edit-capitulo',
        component: () => import(/* webpackChunkName: "demo" */ '@/views/EditarCapitulo.vue')
      },
    ]
  },
  {
    path: '*',
    name: '404',
    component: Erro404
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
